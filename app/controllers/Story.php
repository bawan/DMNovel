<?php

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 16-4-6
 * Time: 上午9:24
 */
class Story extends CI_Controller {

    public $title, $style;

    function __construct() {
        parent::__construct();
        $this->load->model('story_model', 'story');
        $this->load->model('chapter_model', 'chapter');
        $this->style = get_cookie('style')?'bootstrap/'.get_cookie('style'):'bootstrap.min';
        echo $this->style;
    }

    public function index($id) {
        if (!$id) {
            show_error('请输入书号');
        }

        $this->load->model('category_model', 'category');

        $data['categories'] = $this->category->get();

        $data['story']       = $this->story->get($id);
        $data['title']       = $data['story']['title'];
        $data['chapters']    = $this->chapter->get(null, $id);
        $data['category_id'] = $data['story']['category'];

        $data['last_read'] = $this->input->cookie($id) ? json_decode($this->input->cookie($id), true) : '';
        $data['style']     = $this->style;

        $this->load->view('story', $data);
    }

}